zts1e	Najlepsza obs�uga
zts2e	Medal Erenii
zts3e	[Efekt g��wny ] [n] Ka�dy ekwipunek zostanie o 8 zwi�kszony, w ca�o�ci o 24,[n] je�li b�dzie u�yty z medalem poszukiwacza przyg�d, to wtedy b�dzie zwi�kszony o 36 [n] [Efekt dodatkowy] Do�wiadczenie w bitwie zostanie zwi�kszone o 15%.[n] Do�wiadczenie pracy zostanie zwi�kszone o 15%.[n] Punkty do�wiadczenia zwierzaka i przyjaciela zostan� zwi�kszone o 15%. (Zostan� zwi�kszone o 30%, je�li b�dzie u�yty razem z medalem poszukiwacza przyg�d) Wygrana w z�ocie zwi�kszona o 20%.[n] Reputacja zwi�kszona o 20%.
zts4e	Masz ochot� na �atwe wyzwanie?
zts5e	Medal poszukiwacza przyg�d
zts6e	[Efekt g��wny] [n] Wszystkie schowki zostan� rozszerzone o 4, w sumie o 12,[n] je�eli zostanie u�yty medal Erenii, o 36[n] [Dodatkowy efekt] Do�wiadczenie w walce zostanie zwi�kszone o 15%.[n] Do�wiadczenie w zawodzie zostanie zwi�kszone o 15%.[n] Punkty do�wiadczenia zwierz�cia i partnera zostan� zwi�kszone o 15%. (Zostan� zwi�kszone o 30%, je�eli zostanie zastosowany jednocze�nie medal Erenii)
zts7e	Spodoba�a si� lepsza wr�ka?
zts8e	Sellaim
zts9e	Duch ognia; Mo�e rozwin�� si� do 70.[n] Posiada takie same umiej�tno�ci jak wr�ka ognia[n] Tego przedmiotu nie mo�na da� innej osobie.[n]
zts10e	Silniejszy element wody
zts11e	Woondine, wr�ka wody
zts12e	Duch wody; Mo�e rozwin�� si� do 70.[n] Posiada takie same umiej�tno�ci jak wr�ka wody[n] Nie mo�na go sprzeda�[n]
zts13e	Silniejszy element �wiat�a
zts14e	Eperial, wr�ka �wiat�a
zts15e	Duch �wiat�a; Mo�e rozwin�� si� do 70.[n] Posiada takie same umiej�tno�ci jak wr�ka �wiat�a[n] Nie mo�na go sprzeda�[n]
zts16e	Poka�e Ci si�� mroku
zts17e	Turik, wr�ka mroku
zts18e	Duch mroku; Mo�e rozwin�� si� do 70.[n] Posiada takie same umiej�tno�ci jak wr�ka mroku[n] Tego przedmiotu nie mo�na da� innej osobie.[n]
zts19e	Potrzebujesz szybkiego wzrostu?
zts20e	Du�y Sellaim
zts21e	Duch ognia; Mo�e rozwin�� si� do 70.[n] Posiada takie same umiej�tno�ci jak wr�ka ognia[n] Tego przedmiotu nie mo�na da� innej osobie.[n]
zts22e	Gotowy do dzia�ania!
zts23e	Du�y Woondine
zts24e	Duch wody; Mo�e rozwin�� si� do 70.[n]Ma ju� 40 poziom wi�c nie trzeba rozwija� go od 1 poziomu.[n] Posiada takie same umiej�tno�ci jak wr�ka wody[n] Tego przedmiotu nie mo�na da� innej osobie.[n]
zts25e	Zbyt niecierpliwy?
zts26e	Du�y Eperial
zts27e	�wiatlo Ducha ; Mo�e wzrosn�� do 70.[n]Jest ju� 40, wi�c nie musi wzrasta� od 1.[n] Ma te same w�ac�ciwo�ci co Wr�ka �wiat�a[n] Brak mo�liwo�ci handlu[n]
zts28e	Do natychmiastowej akcji!
zts29e	Du�y Turik
zts30e	Cie� ducha ; Mo�e wzrosn�� do 70.[n]Ma ju� 40, nie musi wi�c od 1 wzrasta�.[n] Ma te same w�a�ciwo�ci co cie� czarodziejki[n] Brak mo�liwo�ci handlu[n]
zts31e	Magazyn do wynaj�cia
zts32e	Ekstra du�y sklep(30d)
zts33e	Pozwala ci u�ywa� 56 miejsc w twoim MiniLandzie.[n]Odliczanie zaczyna si� przy rozpocz�ciu ustawiania.[n][Ostrze�enie] Sklep zniknie, kiedy czas si� sko�czy, wiec przygotuj nowy sklep,[n] je�li sklep jest mniejszy, nie wszystkie przedmioty mog� zosta� przej�te. Posegreguj wi�c twoje przedmioty.
zts34e	Daj swojemu zwierzakowi ksyw�!
zts35e	Szyld z imieniem dla NosKumpla
zts36e	Daj twojemu zwierzakowi imi�.[n]Zniknie po u�yciu[n]
zts37e	Nazwij swoje zwierz�
zts38e	5 Szyld�w z imieniem dla NosKumpla
zts39e	Daj twojemu zwierzakowi imi�.[n] Kup 1 zestaw z 5 wisiorkami imion dla twojego zwierzaka[n]
zts40e	Dla szlachetnego zwierzaka
zts41e	3 najlepsze pokarmy NosKumpla
zts42e	Lojalno�� zwierzaka b�dzie maksymalna.[n] Dodatkowe podwy�szenie stopnia o 1[n] Tylko do u�ytku, je�li stopie� zwierzaka znajduje si� 5 stopni poni�ej stopnia gracza[n] (Je�li gracz znajduje si� na poziomie stopnia 50, mo�e zosta� do 45 zwi�kszone)[n]
zts43e	Uwa�aj na swoje zdrowie
zts44e	Medycyna przyjaciela
zts45e	Zaufanie zwierzaka b�dzie maksymalne.[n] Dodatkowe podwy�szenie stopnia o 1[n] Tylko do u�ytku, je�li stopie� zwierzaka znajduje si� poni�ej stopnia gracza[n] (Je�li gracz jest na poziomie stopnia 50, mo�e zosta� do 45 zwi�kszone)[n]
zts46e	Nie zostawiaj swojego zwierzaka samego
zts47e	Per�a Zwierzaka
zts48e	Mo�esz swojego zwierzaka wsadzi� do kuli i trzyma� je w swoim ekwipunku albo sklepie[n] Mo�e by� wymieniane z innymi graczami[n] [Ostrze�enie] Szczeg�y twojego zwierzaka pozostaj�, ale jego imi� zostanie usuni�te.[n] Je�li stopie� zwierzaka jest wy�szy, ni� gracza, mo�e by� wtedy trzymane w MiniLandzie, ale nie mo�e by� NosMatem.[n]
zts49e	Zamie� twojego zwierzaka
zts50e	Per�a Zwierzaka
zts51e	Potrafi umie�ci� Twojego kumpla w perle[n] i przechowywa� go w ekwipunku lub magazynie. Per�a mo�e by� przehandlowana z innymi u�ytkownikami.[n][Jak u�ywa�][n]Wybierz kumple, kt�rego chcesz umie�ci� w perle i kliknij dwa razy na per��.[n][Informacja]Po umieszczeniu kumpla w perle, utracisz jego imi�.[n]Je�li partner ma poziom wy�szy od Twojego, [n]mo�esz go zostawi� w Mini-Land, jednak nie mo�esz go zabra� ze sob�.[n][n]
zts52e	Handluj specjaln� kart�!
zts53e	Pojemnik na Karty Specjalisty
zts54e	Mo�na trzyma� tylko jedn� kart�.[n] Umo�liwia Ci to handel z innymi graczami[n] Poziom i do�wiadczenie s� chronione[n]
zts55e	Specjalny pakiet specjalist�w
zts56e	Ma�e specjalne do�adowanie
zts57e	Wszystkie HP s� przywr�cone.[n] SP zostan� zwi�kszone dodatkowo o 30.000[n]
zts58e	�rednie specjalne do�adowanie
zts59e	Wszystkie HP s� przywr�cone.[n] SP zwi�kszone dodatkowo o 70.000[n]
zts60e	Luksusowy pakiet specjalist�w
zts61e	Du�e specjalne do�adowanie
zts62e	Wszystkie HP/MP zostan� przywr�cone i wszystkie efekty uboczne zostan� usuni�te.[n] SP zostan� dodatkowo zwi�kszone o 180.000[n]
zts63e	Sprawia, �e zak�ady s� bezpieczne!
zts64e	Amulet boskiego b�ogos�awie�stwa
zts65e	Mo�e zosta� u�yty 3 razy[n] Zwi�ksza prawdopodobie�stwo wygranej w zak�adach o 10% i chroni bro� przed defektem[n] Mo�e by� u�ywany do broni i uzbrojenia[n]
zts66e	Zak�adaj si� bez nerw�w!
zts67e	Amulet boskiej ochrony
zts68e	Mo�e zosta� u�yty 3-krotnie[n] Chroni bro� przy nieudanym zak�adzie[n] Mo�e by� stosowany do broni i uzbrojenia[n]
zts69e	Ponowne u�ycie uzbrojenia
zts70e	Amulet wzmacniaj�cy
zts71e	Mo�e zosta� u�yty 3-krotnie. [n] Mo�e polepsza� uzbrojenie, r�wnie� przy sta�ym poziomie.[n] Mo�e zosta� u�yty do broni i uzbrojenia[n]
zts72e	Do pe�na, prosz�!
zts73e	99 Du�y kompletny nap�j
zts74e	Kup 99 du�y kompletny nap�j[n]
zts78e	Ten nap�j jest dost�pny tylko tutaj!!
zts79e	70 woda �r�dlana cylloanu
zts80e	Przywraca z powrotem 3.000 HP/MP
zts84e	T�skni� za tob�!
zts85e	10 Skrzyd�a przyja�ni
zts86e	Mo�e teleportowa� si� do ka�dego, kto jest zarejestrowany jako tw�j przyjaciel.[n] Mo�e zosta� u�yty jedynie w�wczas, gdy przyjaciel znajduje si� na zwyk�ym polu.[n]Aby si� teleportowa�, naci�nij po prostu przycisk 'Przenie�' w komunikatorze.[n]
zts87e	Przystr�j siebie i swoich partner�w!!!
zts88e	Fryzura Wosk A, wygl�dasz super!!
zts89e	To jest przyk�ad wojownika.[n] S� inne wizerunki przedstawiaj�ce �ucznika [n][n] Ye~~ Musz� odr�nia� si� od innych. Ta sama fryzura? Nie, dzi�kuj� ~~ Jestem wyj�tkowy i moja fryzura r�wnie�. Zr�bmy z w�os�w dzie�o sztuki[n] Poszukiwacze przyg�d nie mog� z tego korzysta�.
zts90e	�uczniku Wosk A, jeste� moim Skarbem!
zts91e	To jest przyk�ad �ucznika.[n] S� inne wizerunki przedstawiaj�ce wojownika i maga [n][n] Ye~~ Musz� odr�nia� si� od innych. Ta sama fryzura? Nie, dzi�kuj� ~~ Jestem wyj�tkowy i moja fryzura r�wnie�. Zr�bmy z w�os�w dzie�o sztuki[n] Poszukiwacze przyg�d nie mog� z tego korzysta�.
zts92e	Magu Wosk A, jeste� najlepszy!
zts93e	To jest przyk�ad maga.[n] S� inne wizerunki przedstawiaj�ce wojownika i �ucznika[n][n] Ye~~ Musz� odr�nia� si� od innych. Ta sama fryzura? Nie, dzi�kuj� ~~ Jestem wyj�tkowy i moja fryzura r�wnie�. Zr�bmy z w�os�w dzie�o sztuki[n] Poszukiwacze przyg�d nie mog� z tego korzysta�.
zts94e	Ty przystojniaczku
zts95e	Wojownik, Wosk B
zts96e	To jest przyk�ad wojownika.[n] S� inne wizerunki przedstawiaj�ce �ucznika i maga [n][n] Ye~~ Musz� odr�nia� si� od innych. Ta sama fryzura? Nie, dzi�kuj� ~~ Jestem wyj�tkowy i moja fryzura r�wnie�. Zr�bmy z w�os�w dzie�o sztuki[n] Poszukiwacze przyg�d nie mog� z tego korzysta�.
zts97e	�uczniku, Wosk B!
zts98e	Magu, Wosk B!
zts99e	Nowy kolor w�os�w
zts100e	Czerwona farba do w�os�w
zts101e	Farbuje w�osy na czerwono [n]
zts102e	Zmienia kolor twoich w�os�w!
zts103e	Zielona farba do w�os�w
zts104e	Farbuje w�osy na zielono [n]
zts105e	Gdzie zleci�e� wykonanie tego?
zts106e	Bia�a farba do w�os�w
zts107e	Farbuje w�osy na bia�o [n]
zts108e	Rewolucja w kolorach w�os�w!
zts109e	Niebieska farba do w�os�w
zts110e	Farbuje w�osy na niebiesko [n]
zts111e	Bardzo dobrze ci pasuje!
zts112e	Czarna farba do w�os�w
zts113e	Farbuje w�osy na czarno [n]
zts114e	Kim jeste�?
zts115e	W�asny opis
zts116e	Stw�rz w�asny opis, kt�ry mo�esz pokazywa� innym [n](B�dzie on wy�wietlany, gdy naci�niesz prawy przycisk)[n] Maksymalna d�ugo�� 60 liter[n]
zts117e	To dociera do wszystkich
zts118e	12 G�o�nik�w
zts119e	Mo�esz skierowa� swoje s�owa do wszystkich na serwerze. [n] Twoje s�owa b�d� wy�wietlane w oknie czatu, [n] [Jak u�ywa�] Kliknij dwa razy spiker i wprowad� swoj� wiadomo��.
zts120e	Przeka� ca�emu �wiatu
zts121e	Balon mowy
zts122e	Raz stworzony przechowuje tekst przez 30minut.[n] Jednak�e utworzonej wiadomo�ci nie mo�na ju� zmieni�[n]i po roz��czeniu zniknie[n] [Jak to dzia�a?] Dwa razy kliknij na balon aby wprowadzi� wiadomo��.[n] Je�li przed zamkni�ciem okna wprowadzisz nowy tekst stara wiadomo�� zniknie.
zts123e	Czy nie pragn��e� od dawna aby sta� si� kim� innym?
zts124e	Str�j dla Boba
zts125e	Sprawia, �e Bob dobrze wygl�da [n] i zwi�ksza szybko�� o 2. [n] Jednak ubranie to mo�e znikn�� po umieszczeniu go w perle. [n]
zts126e	Haha... Zostawiasz mnie na lodzie?
zts127e	Str�j Toma
zts128e	Sprawia, �e Tom dobrze wygl�da [n] i zwi�ksza szybko�� o 2. [n] Jednak ubranie to mo�e znikn�� po umieszczeniu go w perle. [n]
zts129e	Pies z rodowodem!
zts130e	Str�j dla Kliffa
zts131e	Sprawia, �e Kliff dobrze wygl�da [n] i zwi�ksza szybko�� o 2. [n] Jednak ubranie to mo�e znikn�� po umieszczeniu go w perle. [n]
zts132e	Zwi�ksza podstawowe PS przez 30 dni
zts133e	Order eksperta
zts134e	Dobra wiadomo�� dla wszystkich, kt�rzy przynajmniej przez godzin� dziennie s� Specjalistami.[n][n] Dzienne PS wynosz� teraz 10.000 + 30.000 [n] W sumie 40.000 punkt�w (Je�li nie wykorzystasz wszystkich, zaoszcz�dzisz 10%) Ciesz si� 40.000 punkt�w przez 30 dni.
zts135e	Event Prezent od sekretnego partnera
zts136e	Medal Przyja�ni
zts137e	Amulet z Eventu Tajemniczy Partner[n] Je�li zostanie u�yty zwi�ksza do�wiadczenie postaci/profesji o 5%.[n] Je�li jeste� w grupie z osobami, kt�re r�wnie� maj� ten amulet zyskasz kolejne 5%.[n] Mo�na u�ywa� przez 10 dni.
zts138e	My�la�em, �e to wystarczy
zts139e	99 Ziaren mocy
zts140e	Kup 99 ziaren mocy [n]
zts141e	Gdzie podzia� si� twoja godno��?
zts142e	3 Eliksiry godno�ci
zts143e	Przywraca godno�� do poziomu 100
zts144e	Sprawia, �e ulepszanie i obstawianie jest bezpieczne!
zts145e	Zw�j ochrony wyposa�enia
zts146e	Mo�na natychmiast ulepsza� lub obstawia�[n] Nawet je�li si� nie powiedzie to przedmiot nie zniknie.[n] Je�li przy obstawianiu uzyskasz ni�szy poziom od oryginalnego,[n] to zostanie zachowany poprzedni poziom.[n] Otwiera okno do natychmiastowego ulepszania luz obstawiania.[n][n] Normalnie wykorzystuje si� dla broni i wyposa�enia.[n] Niezale�nie czy do ulepszania czy obstawiania zu�ywa si� jedno.
zts147e	Zw�j zniesienia
zts148e	Pozwala na ulepszanie wyposa�enia ze sta�ym poziomem.[n] Kliknij dwa razy na zw�j a nast�pnie wybrany przedmiot, [n] sta�y poziom zostanie odblokowany.[n] Wykorzystuje si� do broni i wyposa�enia[n]
zts149e	5 Zwoj�w ochrony wyposa�enia
zts150e	5 Zwoj�w odblokowania
zts151e	Najnowsza moda!
zts152e	Koci kapelusz
zts153e	Odporno�� na wszystkie elementy zwi�kszona o 1[n] Si�a wszystkich element�w zwi�kszona o 10[n]
zts154e	Wyb�r specjalny
zts155e	O�li kapelusz
zts156e	Chroni przed krzykiem Mandry z okre�lonym prawdopodobie�stwem [n] P� maksymalnie zwi�kszone o 100[n] PM maksymalnie zwi�kszone o 50[n]
zts157e	�wietnie w nim wygl�dasz
zts158e	Kowbojski kapelusz
zts159e	Prawdopodobie�stwo krytycznego trafienia zwi�ksza si� o 2%[n] Obra�enia zadawane przez krytyczne trafienie wzrastaj� o 10.[n] Prawdopodobie�stwo, �e otrzymasz krytyczne trafienie zmniejsza si� o 1%[n]
zts160e	Urocze
zts161e	Ma�pi kapelusz
zts162e	Ze znikomym prawdopodobie�stwem chroni ci� przed specjalnymi efektami.[n] Chroni przed negatywnymi efektami 2 poziomu z prawdopodobie�stwem 10%.
zts163e	Sprawia, �e wygl�dasz na silniejszego!
zts164e	Kapelusz z antylopy
zts165e	Maksymalne P� zwi�kszone o 200.[n] Maksymalne PM zwi�kszone o 200.[n]
zts166e	Ze wspania�ym zapachem!
zts167e	Kapelusz porannej bryzy
zts168e	Prawdopodobie�stwo otrzymania krytycznego trafienia zmniejszone o 2%.[n] Je�li otrzymasz krytyczne trafienie jego si�a zostanie zmniejszona o 20%.
zts169e	Silny jak pingwin?
zts170e	Kapelusz pingwina
zts171e	Obrona zwi�ksza si� o 15.[n] Atak zwi�ksza si� o 15.[n]
zts172e	Wykazuje antyczny styl
zts173e	Okrycie g�owy d�entelmena
zts174e	Zmniejsza obra�enia od magicznych atak�w o 4% z prawdopodobie�stwem 8%.[n] Zmniejsza obra�enia od dalekich atak�w o 4%.[n]
zts175e	Uroczy i silny
zts176e	Opaska z du�ymi uszami
zts177e	Zapobiega specjalnym efektom z du�ym prawdopodobie�stwem[n] Zwi�ksza morale o 5[n]
zts178e	Czy kochasz natur�?
zts179e	Kapelusz kwiat doniczkowy
zts180e	Dodaje 70 do obra�e� przeciwko potworom o wysokich poziomach[n] Dodaje 100 do obra�e� przeciwko zwierz�tom o wysokich poziomach.[n]
zts181e	Nauczy Twojego zwierzaka bystro�ci
zts182e	Karma dla bystrego zwierzaka
zts183e	Jak tylko zwierzak zje tan przedmiot, natychmiast zbiera wszystkie le��ce przedmioty. Nie trzeba karmi� go ca�y czas[n] Karmienie: Kliknij zwierz� a nast�pnie dwa razy kliknij na przedmiocie[n][n][Dzia�anie] Tw�j zwierzak automatycznie podnosi upuszczane przedmioty[n](Funkcjonuje tylko w trybie D). Podnosi tylko twoje przedmioty[n] Mo�esz je r�wnie� sam zbiera�.[n]Przedmioty trafiaj� do twojego ekwipunku[n][n] [Uwaga] Je�li umie�cisz zwierz� w Perle funkcja ta zostanie przerwana.
zts184e	Odporno�� na wszystkie elementy zwi�kszona o1 [n] Si�a wszystkich element�w zwi�kszona o 10[n] Tego przedmiotu nie mo�na da� innemu graczowi.[n]
zts185e	Chroni ci� przed okrzykiem Mandry.[n] P� zwi�ksza maksymalnie o 100[n] PM zwi�ksza maksymalnie o 50[n] Tego przedmiotu nie mo�na przekaza� innemu graczowi.[n]
zts186e	Prawdopodobie�stwo zadania krytycznego trafienia jest zwi�kszone o 2%[n] Zwi�ksza obra�enia zadawane przy krytycznym trafieniu o 10%.[n] Prawdopodobie�stwo otrzymania krytycznego trafienia jest zmniejszone o 1%[n] Tego przedmiotu nie mo�na da� innemu graczowi.[n]
zts187e	Z du�ym prawdopodobie�stwem ochrania ci� przed specjalnymi efektami.[n] Chroni przed negatywnymi efektami 2 poziomu z prawdopodobie�stwem 10%. [n]Tego przedmiotu nie mo�na da� innemu graczowi.[n]
zts188e	Maksymalne P� zwi�ksza si� o 200.[n]Maksymalne PM zwi�ksza si� o 200.[n]Przedmiot ten nie mo�e by� sprzedany[n]
zts189e	Zmniejsza prawdopodobie�stwo otrzymania krytycznego trafienia o 2%.[n] Je�li zostaniesz trafiony zmniejsza krytyczne obra�enia o 20%.[n]Tego przedmiotu nie mo�na da� innej osobie.[n]
zts190e	Ca�a obrona jest zwi�kszona o 15.[n] Wszystkie ataki s� zwi�kszone o 15[n] Tego przedmiotu nie mo�na da� innej osobie[n]
zts191e	Z prawdopodobie�stwem 8% zmniejsza obra�enia zadawane przez magiczne ataki o 4% .[n] Zmniejsza obra�enia od dalekich atak�w o 4%.[n] Tego przedmiotu nie mo�na da� innej osobie[n]
zts192e	Zapobiega specjalnym efektom z du�ym prawdopodobie�stwem[n] Zwi�ksza morale o 5[n]Tego przedmiotu nie mo�na da� innej osobie[n]
zts193e	Dodaje 70 do obra�e� przeciwko potworom o wysokich poziomach[n] Dodaje 100 do obra�e� przeciwko zwierz�tom o wysokich poziomach.[n]Tego przedmiotu nie mo�na da� innej osobie[n]
zts194e	Nie u�ywa�
zts195e	Nie u�ywa�
zts196e	Nie u�ywa�
zts197e	Nie u�ywa�
zts198e	Nie u�ywa�
zts199e	Nie u�ywa�
zts200e	Nie u�ywane
zts201e	Nie u�ywane
zts202e	Nie u�ywane
zts203e	Nie u�ywane
zts204e	Niezniszczalny!
zts205e	Olbrzymi magazyn (&#8734;)
zts206e	56 miejsc [n] Mo�na z niego korzysta� przez ca�y czas. [n][n]
zts207e	Wyposa� sw�j taras
zts208e	Nie u�ywane
zts209e	Nie u�ywane
zts210e	Nie u�ywane
zts211e	Nie u�ywane
zts212e	Nie u�ywane
zts213e	Nie u�ywane
zts214e	Nie u�ywane
zts215e	Nie u�ywane
zts216e	Nie u�ywane
zts217e	Nie u�ywane
zts218e	Nie u�ywane
zts219e	Nie u�ywane
zts220e	Nie u�ywane
zts221e	Duch ognia; Mo�e rozwin�� si� do 70.[n] Posiada takie same umiej�tno�ci jak wr�ka ognia[n] Tego przedmiotu nie mo�na da� innej osobie[n]
zts222e	Duch wody; Mo�e rozwin�� si� do 70.[n] Posiada takie same umiej�tno�ci jak wr�ka wody[n] Tego przedmiotu nie mo�na da� innej osobie.[n]
zts223e	Duch �wiat�a; Mo�e rozwin�� si� do 70.[n] Posiada takie same umiej�tno�ci jak wr�ka �wiat�a[n] Tego przedmiotu nie mo�na da� innej osobie.[n]
zts224e	Duch mroku; Mo�e rozwin�� si� do 70.[n] Posiada takie same umiej�tno�ci jak wr�ka mroku[n] Tego przedmiotu nie mo�na da� innej osobie.[n]
zts225e	Duch ognia; Mo�e rozwin�� si� do 70.[n]Ma ju� 40 poziom wi�c nie trzeba rozwija� go od 1 poziomu.[n] Posiada takie same umiej�tno�ci jak wr�ka ognia[n] Tego przedmiotu nie mo�na da� innej osobie.[n]
zts226e	Duch wody; Mo�e rozwin�� si� do 70.[n]Ma ju� 40 poziom wi�c nie trzeba rozwija� go od 1 poziomu.[n] Posiada takie same umiej�tno�ci jak wr�ka wody[n] Tego przedmiotu nie mo�na da� innej osobie.[n]
zts227e	Duch �wiat�a; Mo�e rozwin�� si� do 70.[n]Ma ju� 40 poziom wi�c nie trzeba rozwija� go od 1 poziomu.[n] Posiada takie same umiej�tno�ci jak wr�ka �wiat�a[n] Tego przedmiotu nie mo�na da� innej osobie.[n]
zts228e	Duch mroku; Mo�e rozwin�� si� do 70.[n]Ma ju� 40 poziom wi�c nie trzeba rozwija� go od 1 poziomu.[n] Posiada takie same umiej�tno�ci jak wr�ka mroku[n] Tego przedmiotu nie mo�na da� innej osobie.[n]
zts229e	Bliska obrona 4[n] Daleka obrona 4[n] Odporno�� na magie 4[n] Unik 4[n] Odporno�� na wszystkie elementy zwi�kszona o 1[n] Przeznaczony dla Wojownika, �ucznika lub Maga[n] Po za�o�eniu nie mo�na przekaza� innej osobie[n]
zts230e	Bliska obrona 4[n] Daleka obrona 4[n] Odporno�� na krzyk Mandry po za�o�eniu[n] Maksymalne P� zwi�kszone o 100[n] Maksymalne PM zwi�kszone o 50[n] Przeznaczony dla Wojownika, �ucznika lub Maga[n] Po za�o�eniu nie mo�na przekaza� innej osobie[n]
zts231e	Bliska obrona 3[n] Daleka obrona 6[n] Odporno�� na magie 4[n] Unik 2[n] Prawdopodobie�stwo zadania krytycznego trafienia zwi�kszone o 2%[n]Krytyczne obra�enia zwi�kszone o 10%[n]Zmniejsza prawdopodobie�stwo otrzymania krytycznego trafienia o 1%[n] Przeznaczony dla Wojownika, �ucznika lub Maga[n] Po za�o�eniu nie mo�na przekaza� innej osobie[n]
zts232e	Bliska obrona 5[n] Daleka obrona 3[n] Odporno�� na magie 1[n] Zmniejsza prawdopodobie�stwo, �e zostaniesz og�uszony[n] Chroni przed negatywnymi efektami 2 poziomu z prawdopodobie�stwem 10%[n] Przeznaczony dla Wojownika, �ucznika lub Maga[n] Po za�o�eniu nie mo�na przekaza� innej osobie[n]
zts233e	Bliska obrona 13[n] Daleka obrona 13[n] Odporno�� na magie 13[n] Maksymalne P� zwi�kszone o 200[n] Maksymalne PM zwi�kszone o 200[n] Przeznaczony dla Wojownika, �ucznika lub Maga[n] Po za�o�eniu nie mo�na przekaza� innej osobie[n]
zts234e	Bliska obrona 7[n] Daleka obrona 3[n] Unik 2[n] Zmniejsza prawdopodobie�stwo otrzymania krytycznego trafienia o 2%[n] Krytyczne obra�enia zmniejszone o 20%[n]Przeznaczony dla Wojownika, �ucznika lub Maga[n] Po za�o�eniu nie mo�na przekaza� innej osobie[n]
zts235e	Bliska obrona 5[n] Daleka obrona 3[n] Odporno�� na magie 5[n] Unik 5[n] Obrona zwi�kszona o 15[n] Atak zwi�kszony o 15[n]Przeznaczony dla Wojownika, �ucznika lub Maga[n] Po za�o�eniu nie mo�na przekaza� innej osobie[n]
zts236e	Bliska obrona 1[n] Daleka obrona 2[n] Odporno�� na magie 14[n] Z prawdopodobie�stwem 8% zmniejsza magiczne ataki o 4%[n] Z prawdopodobie�stwem 8% zmniejsza dalekie ataki o 4%[n]Przeznaczony dla Wojownika, �ucznika lub Maga[n] Po za�o�eniu nie mo�na przekaza� innej osobie[n]
zts237e	Bliska obrona 12[n] Bardzo ogranicza prawdopodobie�stwo wyst�pienia krwawienia[n] Morale zwi�kszone o 5[n]Przeznaczony dla Wojownika, �ucznika lub Maga[n] Po za�o�eniu nie mo�na przekaza� innej osobie[n]
zts238e	Bliska obrona 2[n] Daleka obrona 10[n] Odporno�� na magie 3[n] Zwi�ksza o 70 obra�enia przeciwko diab�om wysokiego poziomu[n] Przeznaczony dla Wojownika, �ucznika lub Maga[n] Po za�o�eniu nie mo�na przekaza� innej osobie[n]
zts239e	Plecak podr�nika (10 dni)
zts240e	Nie u�ywane
zts241e	Plecak podr�nika (30 dni)
zts242e	Plecak podr�nika (60 dni)
zts243e	Plecak partnera (10 dni)
zts244e	Plecak partnera (30 dni)
zts245e	Plecak partnera (60 dni)
zts246e	Kosz NosKumpla (10 dni)
zts247e	Kosz NosKumpla (30 dni)
zts248e	Kosz NosKumpla (60 dni)
zts249e	Plecak (10dni)
zts250e	Plecak (30 dni)
zts251e	Plecak (60 dni)
zts252e	5 b�ogos�awie� Ereni
zts253e	10 b�ogos�awie� Ereni
zts254e	B�ogos�awie�stwo Anio�a Str� 10szt
zts255e	10 b�ogos�awie�stw Zenas
zts256e	Zestaw b�ogos�awie�stw 5szt
zts257e	Zestaw b�ogos�awie�stw 10szt
zts258e	Trwa�y wznawiaj�cy kupon (40 sztuk)
zts259e	Kupon do�adowywania trwa�o�ci (99 sztuk)
zts260e	Kupon podw�jnej nagrody (10 sztuk)
zts261e	Kupon na punkty produkcji (10 sztuk)
zts262e	Kupon na punkty produkcji (30 sztuk)
zts263e	Tajemnicze pude�ko A
zts264e	Tajemnicze pude�ko B
zts265e	Tajemnicze pude�ko C
zts266e	[n]
zts267e	Przywr�� po prostu znowu wa�no��
zts268e	Mo�e zosta� u�yty, je�li skorzysta si� z przycisku w oknie produkcji 'U�yj kupon'[n] Jeden kupon zwi�ksza wa�no�� o 300[n]
zts269e	Podw�j nagrod� Mini-Gier!
zts270e	Je�li posiadasz kupony w swoim ekwipunku, zostaniesz zapytany czy chcesz je u�y�.[n] Je�li je u�yjesz, dostaniesz podw�jn� nagrod� z wybranego przedmiotu.[n]
zts271e	Produkuj tyle ile chcesz!
zts272e	Punkty produkcji Reaktywator (10 sztuk)
zts273e	Punkty produkcji zostan� zwi�kszone o 500, je�li u�yjesz 1 kupon[n] Maksymalny limit punkt�w produkcji to 2000[n]
zts274e	Punkt produkcji regeneracja 30 sztuk
zts275e	U�yj wszystkich przedmiot�w w ekwipunku.
zts276e	Wszystkie przedmioty w ekwipunku mo�esz u�ywa� przez [n]10 dni.[n] Nie mo�e by� u�yty z amuletem.
zts277e	Mo�esz u�ywa� wszystkie przedmioty w ekwipunku przez 30 [n]dni.[n] Nie mo�e by� u�yte z amuletem.
zts278e	Mo�esz u�ywa� wszystkie przedmioty w ekwipunku przez [n]60 dni.[n] Nie mo�e by� u�yte z amuletem.
zts279e	Ekwipunek tak�e dla przyjaciela.
zts280e	Mo�esz u�ywa� 50 dodatkowych miejsc w ekwipunku twojego przyjaciela.[n]Tylko przez 10 dni[n]
zts281e	Mo�esz u�ywa� 50 dodatkowych miejsc w ekwipunku twojego przyjaciela.[n]Tylko przez 30 dni[n]
zts282e	Mo�esz u�ywa� 50 dodatkowych miejsc w ekwipunku twojego przyjaciela.[n]Tylko przez 60 dni[n]
zts283e	U�ywaj wsz�dzie sklepu
zts284e	Poprzez twoje zwierze mo�esz wsz�dzie otwiera� sklep.[n] Przez 10 dni do u�ytku.[n]
zts285e	Sklep mo�e zosta� wsz�dzie otwarty, je�li u�yje si� zwierzaka.[n] Tylko przez 30 dni[n]
zts286e	U�yj naraz wszystkich przedmiot�w w ekwipunku.
zts287e	Kup naraz plecak wyprawy, plecak przyjaciela i kosz ekstradycji zwierzaka[n]
zts288e	Otrzymaj b�ogos�awie�stwo od Ancelloan.
zts289e	5 b�ogos�awie�stw od Ancelloana
zts290e	Do�wiadczenie bitwy i pracy zosta�y zwi�kszone na jedn� godzin� o [n]100%. Bez ograniczenia stopnia.[n] Nie mo�e zosta� u�yte z amuletem.
zts291e	10x B�ogos�awie�stwo Ancelloan
zts292e	B�ogos�awie�stwo NosKumpla
zts293e	5 b�ogos�awie�stw Anio�a Str�a
zts294e	50% zwi�kszenie do�wiadczenia NosKumpla. Pr�dko�� +2[n] trwa przez 1 godzin�[n]
zts295e	Czar podw�jnej nagrody
zts296e	We� sobie ten �wietny przedmiot!
zts297e	Otrzymaj jedn� z okrutnych masek A, [n]okrutn� mask� B, [n]okrutn� mask� C, [n] per�� krzakogona, [n] per�� gladiatora krzakogona,[n] czarn� per�� deratyzatora krzakogona
zts298e	Otrzymaj jedn� z masek milczenia A, [n]mask� milczenia B, [n]mask� milczenia C, [n]per�� krzakogona, [n] per�� gladiatora krzakogona,[n] per�� deratyzatora krzakogona
zts299e	Otrzymaj opask� otch�ani na oczy A, [n] opask� otch�ani na oczy B,[n] opask� otch�ani na oczy C,[n]per�� krzakogona,[n]per�� gladiatora krzakogona,[n] per�� deratyzatora krzakogona
zts300e	Do�wiadczenie bitwy i pracy zwi�kszone o 100% na 1 godzin�[n] Dla ka�dego stopnia[n]
zts301e	Otrzymaj jedn� z okrutnych masek A, okrutn� mask� B, okrutn� mask� C, kamie� krzakogona, strojny kamie� krzakogona, czarny kamie� krzakogona, [n] 5 Cash przedmiot�w
zts302e	Otrzymaj jedn� z masek milczenia A, mask� milczenia B, mask� milczenia C, kamie� krzakogona, strojny kamie� krzakogona, czarny kamie� krzakogona, [n] 5 Cash przedmiot�w
zts303e	Otrzymaj opask� otch�ani na oczy A, opask� otch�ani na oczy B, opask� otch�ani na oczy C, kamie� krzakogona, strojny kamie� krzakogona, czarny kamie� krzakogona[n] 5 Cash przedmiot�w
zts304e	Tori jako m�j zwierzak
zts305e	Tori Tori pi�ka
zts306e	Rejestracja s�odkiego Tori jako zwierze[n]
zts307e	Gladiator Tori pi�ka
zts308e	Czarna Tori pi�ka
zts309e	Otrzymaj jedna z okrutnych masek A, okrutn� mask� B, okrutn� mask� C,[n] krzakogon, ogonek deratyzatora krzakogona, ogonek gladiatora krzakogona,[n] zw�j ochronny wyposa�enia, zw�j zezwalaj�cy na start stopnia, jedzenie dla m�drych zwierzak�w domowych, wod� �r�dlan� z Cylloanu, du�y nape�niacz PS
zts310e	Otrzymaj jedn� z okrutnych masek A, okrutn� mask� B, okrutn� mask� C,[n] krzakogona, deratyzatora krzakogona, gladiatora krzakogona,[n] zw�j ochronny wyposa�enia, zw�j zezwalaj�cy na start stopnia, karm� dla m�drych zwierzak�w domowych, wod� �r�dlan� z Cylloanu, du�y nape�niacz PS
zts311e	Otrzymaj jedn� z opasek czelu�ci na oczy A, opask� czelu�ci na oczy B, opask� czelu�ci na oczy C,[n] krzakogon, deratyzator krzakogona, gladiatora krzakogona,[n] zw�j ochronny wyposa�enia, zw�j zezwalaj�cy na start stopnia, jedzenie dla m�drych zwierzak�w domowych, wod� �r�dlan� z Cylloanu, du�y nape�niacz PS
zts312e	Zwi�ksz umiej�tno�ci czarodziejki!
zts313e	Wzmacniacz czarodziejki
zts314e	Wzmocnij umiej�tno�ci czarodziejki na 1 godzin�.[n] Je�li u�yjesz wzmacniacza czarodziejek, nie mo�esz na czas dzia�ania zmienia� czarodziejki.
zts357e	Mam takie przeczucie, �e w Nowym Roku si� bardzo wybij�!
zts358e	1 koperta z nagrod�
zts359e	Mo�esz otrzyma� jeden z nast�puj�cych obiekt�w: �redni specjalny uzupe�niacz, b�ogos�awie�stwo od Ancelloana, pismo wyzwolenia, zw�j do ochrony wyposa�enia, � ., Du�y specjalny uzupe�niacz, plecak poszukiwacza przyg�d, wod� �r�dlan� od Cylloanu, du�y kompletny nap�j, he�m czarnego rycerza, mask� czarnego rycerza, mro�n� per�� krzakogona, per�� �winki szcz�cia, per�� szcz�liwego szczura.[n]
zts360e	10 kopert z nagrodami
zts361e	5 kopert z nagrodami
zts362e	1 prezent urodzinowy
zts363e	Skoro tylko tego u�yjesz, b�d� nast�puj�ce elementy przez 15 dni skuteczne:[n][Elementy podstawowe:][n]Posiadasz w swoim ekwipunku w kategoriach "wyposa�enie", "g��wne" i "inne" jednocze�nie 4 miejsca wi�cej (razem 12 miejsc).[n]Otrzymasz 10% wi�cej do�wiadczenia.[n][Inne efekty:][n]Tw�j przyjaciel i tw�j NosKumpel otrzymuj� 10% wi�cej do�wiadczenia.[n]Twoje warto�ci ataku i obrony wzrastaj� o 10%.[n][Ostrze�enie:][n]Je�li u�yjesz tego razem z plecakiem poszukiwacza przyg�d, twoje miejsca w ekwipunku NIE zostan� podwojone![n]Nie mo�esz tego przedmiotu w�o�y� do sklepu.[n][n]
zts364e	Nosi� ma�a myszk�.
zts365e	S�odki kapelusz szczura
zts366e	Mo�esz posiada� szcz�liwego szczura jako zwierz� domowe.[n][n]
zts367e	Pob�ogos�awiony r�ow� �wink�
zts368e	Per�a Nowego Roku!
zts369e	Mo�esz posiada� �wink� szcz�cia jako zwierz� domowe.[n][n]Twoje warto�ci ataku i obrony wzrastaj� o 10%, je�li ono ci b�dzie towarzyszy�.[n]
zts370e	Ch�odzeinie, aby przeciwdzia�a� wypadkom.
zts371e	Krzakogon z jedn� per�� mrozu
zts372e	Mo�esz posiada� krzakogon jako zwierz� domowe.[n][n]Je�li b�dzie atakowa�, zmrozi z du�� pewno�ci� przeciwnika. Pr�dko�� i stopie� ochrony przeciwnika zostan� poprzez to zamro�one i spadn�.[n]
zts373e	Mam mojego w�asnego ma�ego �wi�tego Miko�aja!
zts374e	Per�a krzakogona
zts718e	Kostium dalmaty�czyka
zts719e	Kostium dalmaty�czyka (7 dni)
zts720e	Kostium dalmaty�czyka (30 dni)
zts721e	kostium dalmaty�czyka (wieczny)
zts722e	Kostium rottweilera
zts723e	Kostium rottweilera (7 dni)
zts724e	Kostium rottweilera (30 dni)
zts725e	Kostium rottweilera (wieczny)
zts727e	Kostium kota syjamskiego
zts728e	Kostium kota syjamskiego (7 dni)
zts729e	Kostium kota syjamskiego (30 dni)
zts730e	Kostium kota syjamskiego (wieczny)
zts731e	Kostium szarego kota
zts732e	Kostium szarego kota (7 dni)
zts733e	Kostium szarego kota (30 dni)
zts734e	Kostium szarego kota (wieczny)
zts736e	Kostium kr�liczka
zts737e	Kostium kr�liczka (7 dni)
zts738e	Kostium kr�liczka (30 dni)
zts739e	Kostium kr�liczka (wieczny)
zts740e	��ty kostium kr�liczka
zts741e	��ty kostium kr�liczka (7 dni)
zts742e	��ty kostium kr�liczka (30 dni)
zts743e	��ty kostium kr�liczka (wieczny)
zts749e	Jasny kostium lwa
zts750e	Jasny kostium lwa (7 dni)
zts751e	Jasny kostium lwa (30 dni)
zts752e	Jasny kostium lwa (wieczny)
zts753e	Kostium buldoga
zts754e	Kostium buldoga (7 dni)
zts755e	Kostium buldoga (30 dni)
zts756e	Kostium buldoga (wieczny)
zts757e	Kostium bernardyna
zts758e	Kostium bernardyna (7 dni)
zts759e	Kostium bernardyna (30 dni)
zts760e	Kostium bernardyna (wieczny)
zts761e	Kostium birma�skiego kota
zts762e	Kostium birma�skiego kota (7 dni)
zts763e	Kostium birma�skiego kota (30 dni)
zts764e	Kostium birma�skiego kota (wieczny)
zts765e	Kostium kota Korat
zts766e	Kostium kota Korat (7 dni)
zts767e	Kostium kota Korat (30 dni)
zts768e	Kostium kota Korat (wieczny)
zts770e	Kostium marcowego kr�lika
zts771e	Kostium marcowego kr�lika (7 dni)
zts772e	Kostium marcowego kr�lika (30 dni)
zts773e	Kostium marcowego kr�lika (wieczny)
zts774e	Bia�y kostium kr�lika
zts775e	Bia�y kostium kr�lika (7 dni)
zts776e	Bia�y kostium kr�lika (30 dni)
zts777e	Bia�y kostium kr�lika (wieczny)
zts778e	Z�oty kostium lwa
zts779e	Z�oty kostium lwa (7 dni)
zts780e	Z�oty kostium lwa (30 dni)
zts781e	Z�oty kostium lwa (wieczny)
zts783e	Czapka Dalmaty�czyka
zts784e	Czapka Dalmaty�czyka (7 dni)
zts785e	Kapelusz dalmaty�czyka (30 dni)
zts786e	Czapka Dalmaty�czyka (na sta�e)
zts787e	Czapka Rottweilera
zts788e	Czapka Rottweilera (7 dni)
zts789e	Czapka Rottweilera (30 dni)
zts790e	Czapka Rottweilera (na sta�e)
zts792e	Czapka Siamese
zts793e	Czapka Siamese (7 dni)
zts794e	Kapelusz syjamskiego kota (30 dni)
zts795e	Kapelusz syjamskiego kota (wieczny)
zts796e	Kapelusz szarego kota
zts797e	Kapelusz szarego kota (7 dni)
zts798e	Kapelusz szarego kota (30 dni)
zts799e	Kapelusz szarego kota (wieczny)
zts801e	Uszy kr�lika
zts802e	Uszy kr�lika (7 dni)
zts803e	Uszy kr�lika (30 dni)
zts804e	Uszy kr�lika (wieczne)
zts805e	Z�ote uszy kr�lika
zts806e	Z�ote uszy kr�lika (7 dni)
zts807e	Z�ote uszy kr�lika (30 dni)
zts808e	Z�ote uszy kr�lika (wieczne)
zts814e	Jasny lwi kapelusz
zts815e	Jasny lwi kapelusz (7 dni)
zts816e	Jasny lwi kapelusz (30 dni)
zts817e	Jasny lwi kapelusz (wieczny)
zts818e	Kapelusz buldoga
zts819e	Kapelusz buldoga (7 dni)
zts820e	Kapelusz buldoga (30 dni)
zts821e	Kapelusz buldoga (wieczny)
zts822e	Kapelusz bernardyna
zts823e	Kapelusz bernardyna (7 dni)
zts824e	Kapelusz �wi�tego Bernarda (30 dni)
zts825e	Kapelusz bernardyna (wieczny)
zts826e	Kapelusz birma�skiego kota
zts827e	Kapelusz birma�skiego kota (7 dni)
zts828e	Kapelusz birma�skiego kota (30 dni)
zts829e	Kapelusz birma�skiego kota (wieczny)
zts830e	Kapelusz kota Korat
zts831e	Kapelusz kota Korat (7 dni)
zts832e	Kapelusz kota Korat (30 dni)
zts833e	Kapelusz kota Korat (wieczny)
zts835e	Kapelusz marcowego kr�lika
zts836e	Kapelusz marcowego kr�lika (7 dni)
zts837e	Kapelusz marcowego kr�lika (30 dni)
zts838e	Kapelusz marcowego kr�lika (wieczny)
zts839e	Bia�y kr�liczy kapelusz
zts840e	Bia�y kr�liczy kapelusz (7 dni)
zts841e	Czapka Bia�ego Kr�lika (30 dni)
zts842e	Bia�y kr�liczy kapelusz (wieczny)
zts843e	Z�oty lwi kapelusz
zts844e	Z�oty lwi kapelusz (7 dni)
zts845e	Czapka Z�otego Lwa (30 dni)
zts846e	Z�oty lwi kapelusz (wieczny)
zts899e	Czapka Dalmaty�czyka
zts900e	Czapka Dalmaty�czyka (7 dni)
zts901e	Czapka Dalmaty�czyka (30 dni)
zts902e	Czapka Dalmaty�czyka (na sta�e)
zts903e	Czapka Rottweilera
zts904e	Czapka Rottweilera (7 dni)
zts905e	Czapka Rottweilera (30 dni)
zts906e	Czapka Rottweilera (na sta�e)
zts908e	Jasny lwi kapelusz
zts909e	Jasny lwi kapelusz (7 dni)
zts910e	Jasny lwi kapelusz (30 dni)
zts911e	Jasny lwi kapelusz (wieczny)
zts912e	Ciemny kapelusz lwa
zts913e	Ciemny kapelusz lwa (7 dni)
zts914e	Ciemny kapelusz lwa (30 dni)
zts915e	Ciemny lwi kapelusz (wieczny)
zts916e	Kapelusz buldoga
zts917e	Kapelusz buldoga (7 dni)
zts918e	Kapelusz buldoga ( 30 dni)
zts919e	Kapelusz buldoga (wieczny)
zts920e	Kapelusz bernardyna
zts921e	Kapelusz bernardyna (7 dni)
zts922e	Kapelusz bernardyna (30 dni)
zts923e	Kapelusz bernardyna (wieczny)
zts925e	Z�oty kapelusz lwa
zts926e	Z�oty kapelusz lwa (7 dni)
zts927e	Z�oty lwi kapelusz (30 dni)
zts928e	Z�oty kapelusz lwa (wieczny)
zts929e	Ciemny lwi kapelusz
zts930e	Ciemny lwi kapelusz (7 dni)
zts931e	Ciemny kapelusz lwa (30 dni)
zts932e	Ciemny lwi kapelusz (wieczny)
zts933e	Kostium dalmaty�czyka
zts934e	Kostium dalmaty�czyka (7 dni)
zts935e	Kostium dalmaty�czyka (30 dni)
zts936e	Kostium dalmaty�czyka (wieczny)
zts937e	Kostium rottweilera
zts938e	Kostium rottweilera (7 dni)
zts939e	Kostium rottweilera (30 dni)
zts940e	Kostium rottweilera (wieczny)
zts941e	Wspania�y Kapelusz Lwa!
zts942e	Jasny kostium lwa (7 dni)
zts943e	Jasny kostium lwa (30 dni)
zts944e	Jasny kostium lwa (wieczny)
zts945e	Ciemny kostium lwa (7 dni)
zts946e	Ciemny kostium lwa (30 dni)
zts947e	Ciemny kostium lwa (wieczny)
zts948e	Kostium buldoga
zts949e	Kostium buldoga (7 dni)
zts950e	Kostium buldoga (30 dni)
zts951e	Kostium buldoga (wieczny)
zts952e	Wytworny Kapelusz Lwa!
zts953e	Z�oty kapelusz lwa
zts954e	Z�oty kapelusz lwa (7 dni)
zts955e	Z�oty lwi kapelusz (30 dni)
zts956e	Z�oty kapelusz lwa (wieczny)
zts957e	Ciemny lwi kapelusz
zts958e	Ciemny lwi kapelusz (7 dni)
zts959e	Ciemny kapelusz lwa (30 dni)
zts960e	Ciemny lwi kapelusz (wieczny)
zts961e	Jasny kostium lwa
zts962e	Ciemny kostium lwa
zts963e	Kapelusz bernardyna
zts964e	Kapelusz bernardyna (7 dni)
zts965e	Kapelusz bernardyna (30 dni)
zts966e	Kapelusz bernardyna (wieczny)
zts1005e	5 Eliksir�w PvP si�y ataku
zts1006e	10 Eliksir�w PvP si�y ataku
zts1007e	5 Eliksir�w PvP si�y obrony
zts1008e	10 Eliksir�w PvP si�y obrony
zts1009e	5 Eliksir�w PvP odporno�ci
zts1010e	10 Eliksir�w PvP odporno�ci
zts1011e	5 Rog�w przywo�ania rodziny
zts1012e	10 Rog�w przywo�ania rodziny
zts1057e	Jestem symbolem wiecznego pi�kna!
zts1058e	Dzwoneczek
zts1059e	Dzwoneczek (7 dni)
zts1060e	Dzwoneczek (30 dni)
zts1061e	Kostium Wr�ki (nieograniczony)
zts1062e	Jestem demonem Nostale!
zts1063e	Bia�y demon
zts1064e	Bia�y demon (7 dni)
zts1065e	Bia�y demon (30 dni)
zts1066e	Bia�y demon (wieczny)
zts1067e	Za�� �adn� wst��k�!
zts1068e	R�owa kokarda do w�os�w
zts1069e	R�owa kokarda do w�os�w (7 dni)
zts1070e	R�owa kokarda do w�os�w (30 dni)
zts1071e	R�owa kokarda do w�os�w (wieczna)
zts1072e	Zosta� postawnym m�czyzn�!
zts1073e	Bia�y filcowy kapelusz
zts1074e	Bia�y filcowy kapelusz (7 dni)
zts1075e	Bia�y filcowy kapelusz (30 dni)
zts1076e	Bia�y filcowy kapelusz (wieczny)
zts1118e	Usuwa obce zapachy!
zts1119e	Perfuma
zts1120e	B�dzie u�ywany, je�li przekszta�cisz ekskluzywne przedmioty innych w swoje w�asne.[n][Jak si� tego u�ywa][n] Okno do przekszta�cania przedmiot�w otworzy si�, je�li klikniesz na nie dwa razy
zts1121e	20 perfum
zts1122e	50 perfum
zts1123e	99 perfum
zts1146e	Pegaz
zts1147e	S�odki Zaj�czek
zts1242e	Losowe Noworoczne Pude�ko!
zts1243e	Noworoczne pude�ko
zts1244e	Noworoczne pude�ko[n]Mo�esz otrzyma� jeden z r�norodnych przedmiot�w.[n]Przedmioty w pude�ku[n]Per�a Byka Tori, Tajemnicze Pude�ko B, Koszyk Zwierz�tka (60 dni), 5 Flakonik�w Perfum, 50 Skrzyde� Przyja�ni, 50 W�d �r�dlanych z Cylloan, Plecak Poszukiwacza Przyg�d (30 dni), Per�a NosKumpla, 2 Szyldy z imieniem dla NosKumpla, 3 Porcje Najlepszego Po�ywienia dla NosKumpla.[n][n]
zts1266e	Sprawd� swoje szcz�cie!
zts1325e	Perla chorego krzakogona
zts1327e	Dobrze wygl�daj�cy kapelusz dla m�czyzn
zts1329e	Dobrze wygl�daj�cy kapelusz dla m�czyzn (7 dni)
zts1331e	Dobrze wygl�daj�cy kapelusz dla m�czyzn (nieograniczony)
zts1332e	Dobrze wygl�daj�cy garnitur dla m�czyzn
zts1334e	Dobrze wygl�daj�cy garnitur dla m�czyzn (7 dni)
zts1336e	Dobrze wygl�daj�cy garnitur dla m�czyzn (nieograniczony)
zts1348e	Gwiezdny Mundurek Szkolny
zts1349e	Gwiezdny Mundurek Szkolny (7 dni)
zts1350e	Gwiezdny Mundurek Szkolny (30 dni)
zts1351e	Gwiezdny Mundurek Szkolny (Nieograniczone)
zts1359e	Per�a Samuraja-Krzakogona
zts1394e	Skrzyd�o Anio�a
zts1397e	Skrzyd�o Demona
zts1510e	Pi�ra Anio�a (99 sztuk)
zts1511e	Tajemnicze pud�o A [n]Je�li otworzysz to pud�o, b�dziesz m�g� uzyska� nast�puj�ce przedmioty NosMarketu. [n] [Przedmioty, kt�re mo�esz uzyska�] [n] Maska Grozy A [n] Maska Grozy B [n] Maska Grozy C [n] Per�a Krzakogona[n] Per�a Gladiatora Krzakogona  [n] Per�a Deratyzatora Krzakogona[n] Karma dla bystrego zwierzaka[n] Zwoje Uwolnienia[n] Woda �r�dlana z Cylloanu[n] Zw�j do zniesienia ustalonego poziomu[n] Du�y Specjalny Eliksir[n]
zts1512e	Tajemnicze pud�o B [n] Je�li otworzysz to pud�o, b�dziesz m�g� uzyska� nast�puj�ce przedmioty NosMarketu. [n] [Przedmioty, kt�re mo�esz uzyska�] [n] Maska ciszy A [n] Maska ciszy B [n] Maska ciszy C [n] Per�a byka krzakogona[n] Per�a wodnego krzakogona[n] Per�a Samuraja-Krzakogona[n] Karma dla bystrego zwierzaka[n] Zwoje ochrony wyposa�enia[n] Woda �r�dlana z Cylloanu[n] Zw�j do zniesienia ustalonego poziomu[n] Du�y Specjalny Eliksir[n]
zts1513e	Tajemnicze pud�o C [n]Je�li otworzysz to pud�o, b�dziesz m�g� uzyska� nast�puj�ce przedmioty NosMarketu. [n] [Przedmioty, kt�re mo�esz uzyska�] [n] Przepask� na oczy otch�ani A [n] Przepask� na oczy otch�ani B [n] Przepask� na oczy otch�ani C[n] Per�a Pegaza[n] Per�a s�odkiego zaj�ca[n] Per�a chorego krzakogona[n] Karma dla bystrego zwierzaka[n] Zwoje ochrony wyposa�enia[n] Woda �r�dlana z Cylloanu[n] Zw�j do zniesienia ustalonego poziomu[n] Du�y Specjalny Eliksir[n]
zts1528e	Koszyk zwierz�tka (10 dni, event)
zts1530e	Piecz�� Olbrzymiego Czarnego Paj�ka
zts1531e	Piecz�� Olbrzymiego Slade
zts1532e	Piecz�� Xysenstrauch
zts1533e	Piecz�� Mrocznego Castra
zts1534e	Piecz�� Kr�la Kurczak�w
zts1535e	Piecz�� Matki Cuby
zts1537e	Magiczny skuter
zts1540e	Magiczny Dywan
zts2790e	Medal NosHandlarza (1 dzie�)(event)
zts2800e	Medal NosHandlarza (30 dni)
zts2801e	Medal NosHandlarza (7 dni)
zts8848e	Losowa Skrzynia ME 2012
zts8993e	Losowa Skrzynia Puszka K��buszka
zts9201e	Odjechane Pude�ko Nagr�d
zts9265e	Drobniutki Samorodek Z�ota
zts9266e	Ma�y Samorodek Z�ota
zts9267e	Du�y Samorodek Z�ota
zts9268e	Wielki Samorodek Z�ota
zts9282e	Skrzynia Specjalisty: "Skrzyd�a i nie tylko"
zts10043e	Po��czenie kostium�w
zts10054e	Dla wszystkich, kt�rzy uwielbiaj� zmiany!
zts10055e	Dzi�ki niemu mo�esz zmieni� wygl�d swojego kostiumu.[n]Aby to zrobi�, musisz zarejestrowa� kostium bazowy i kostium materia�owy.[n][Uwaga][n]1. Kostium�w z ograniczeniem czasowym nie mo�na zmienia�.[n]2. Kostiumy przypisane do postaci mo�na zmieni� jeden raz.[n]3. Wskutek tego zniknie kostium materia�owy.[n]4. Poprzez [Reset kostiumu materia�owego] mo�esz cofn�� zmian�.[n]5. Opcje kostiumu materia�owego nie dotycz� kostiumu bazowego.[n]
